<?php
			$listOfActivities= array(
				'HTML and PHP' => array(
					'<a href="PHP/1-1.php">1-1 Show Hello World</a>',
					'<a href="PHP/1-2.php">1-2 The four basic operations of arithmetic</a>',
					'<a href="PHP/1-3.php">1-3 Show greatest common divisor</a>',
					'<a href="PHP/1-4.php">1-4 Solve the FizzBuzz problem</a>',
					'<a href="PHP/1-5.php">1-5 Input date. Then show 3 days from the input date and the days of the week of each date</a>',
					'<a href="PHP/1-6.php">1-6 Input user information. Then show it on the next page</a>',
					'<a href="PHP/1-7(1-8).php">1-(7-8)   Add validation and store in csv file</a>',
					'<a href="PHP/1-9(to-1-12).php">1-(9-12) Show user info using table with uploaded images</a>',
					'<a href="PHP/1-13.php">1-13     Create a login form and embed it into the system that you developed</a>',

				),

				'Javascript' => array(
					'<a href="JavaScript/2-1.html">2-1 Show alert</a>',
					'<a href="JavaScript/2-2.html">2-2 Confirm dialog and redirection</a>',
					'<a href="JavaScript/2-3.html">2-3 The four basic operations of arithmetic</a>',
					'<a href="JavaScript/2-4.html">2-4 Show prime numbers</a>',
					'<a href="JavaScript/2-5.html">2-5 Input characters in the textbox and show it in the label</a>',
					'<a href="JavaScript/2-6.html">2-6 Press the button and add a label below the button</a>',
					'<a href="JavaScript/2-7.html">2-7 Show alert when you click an image</a>',
					'<a href="JavaScript/2-8.html">2-8 Show alert when you click a link</a>',
					'<a href="JavaScript/2-9.html">2-9 Change text and background color when you press buttons</a>',
					'<a href="JavaScript/2-10.html">2-10 Scroll screen when you press buttons</a>',
					'<a href="JavaScript/2-11.html">2-11 Change background color using animation</a>',
					'<a href="JavaScript/2-12.html">2-12 Show another image when you mouse over an image. Then show the original image when you mouse out</a>',
					'<a href="JavaScript/2-13.html">2-13 Change the size of images when you press buttons</a>',
					'<a href="JavaScript/2-14.html">2-14 Show images according to the options in the combo box</a>',
					'<a href="JavaScript/2-15.html">2-15 Show current date and time in real time</a>'
				),

				'Database' => array(
					'<a href="Database/3-2.txt">3-2 Create table</a>',
					'<a href="Database/3-3.txt">3-3 Insert, update, delete query</a>',
					'<a href="Database/3-4.txt">3-4 SQL Problems</a>',
					'<a href="Database/3-5.txt">3-5 NOTE: Execute this SQL queries first, before proceeding below</a>',
					'<a href="Database/3-5(registration).php">3-5 Register, Login, and Display list of users</a>',
				),

				'Advanced SQL' => array(
					'<a href="Advanced%20SQL/4-1.txt">4-1 Selecting by age using birth date</a>',
					'<a href="Advanced%20SQL/4-2.txt">4-2 List all therapists according to their daily work shifts</a>',
					'<a href="Advanced%20SQL/4-3.txt">4-3 Using Case conditional statement</a>',
					'<a href="Advanced%20SQL/4-4.txt">4-4 Create sql that will display the OUTPUT</a>'
				),

				'Practice systems/programs' => array(
					'<a href="Practice%20Systems/6-1/">6-1 Randomized Multiple Choice Quiz</a>',
        			'<a href="Practice%20Systems/6-1/Andy%20Yeboah%20Cudjoe%20(Quiz).pdf">6-1 Wireframe and Schema</a>',
        			'<a href="Practice%20Systems/6-2.php">6-2 Calendar</a>',
        			'<a href="">6-3 This is the navagation page</a>',

				),
			);
		?>
<html>
	<head>
		<style>
			body{
				background: black;
				color: white;
			}
			a{
				text-decoration: none;
				color: white;
			}
		</style>
	</head>
	<body>		
		<?php foreach($listOfActivities as $key => $value){ ?>
			<section aria-labelledby="contact-heading">
				<div>
					<h2 id="contact-heading">
					<?php echo $key; ?>
					</h2>
				</div>
					<dl>
						<?php
							foreach($value as $excercise){
								echo
									'<div>
										<dd>'.
											$excercise.'
										</dd>
									</div>';
							}
						?>
					</dl>
			</section>
		<?php }?>		
	</body>
</html>
