<?php
    include "quizConnection.php";
    $query = $con->prepare("SELECT * FROM `data` ORDER BY RAND()");
    $query->execute();
    $response = $query->get_result();
    $links = $slides = $options = $choices = "";
    if ($response->num_rows > 0) {
        $num = 0;
        while ($row = $response->fetch_assoc()) {
            $num++;
            $links .= '<a href="#slide-'.$num.'">'.$num.'</a>';
            
            $con1 = [1,2,3,4];
            shuffle($con1);
            for ($i=1; $i <= 4; $i++) { 
                $choice = "choice_".$con1[$i-1];
                ($i == 1)? $required="required":"";
                $options .= '
                            <input type="hidden" name="qid'.$num.'" value="'.$row["id"].'">
                            <label class="option option-'.$i.'">
                                <input type="radio" name="answer'.$num.'" class="optionbox" id="option-'.$num.$i.'" value="'.$row[$choice].'" '.$required.'>
                                <span>'.wordwrap($row[$choice], 25, "<br />\n").'</span>
                            </label>';
            }

            $slides .= '
                <div id="slide-'.$num.'">
                    <table>
                        <tr>
                            <td colspan="2">   
                                <div class="titleblock">Question #'.$num.'</div>
                                <textarea name="txtQuestion'.$row["id"].'" rows="5" placeholder="Enter question #'.$row["id"].' here..." disabled required>'.$row["question"].'</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="wrapper">
                                    '.$options.'
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            ';
            $options = $choices = "";
        }
    } else {
        header("Location: quizQuestionare.php");
    }
    $con->close();
?>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <style>
            .titleblock,
            table th div,
            button,
            .slider .links>a {
                background:#1c1e29!important;
            }
            .links>a{
                border-radius: 25%!important;
                height: 41px !important; 
            }
        </style>
    </head>
    <body>
        <main>
            <form method="post" action="quizScoreResult.php">
                <div class="title">
                    <hr>
                    <h1>RIDDLEY RIDDLE QUIZ</h1>
                    <hr>
                </div>
                <div class="slider">
                    <div class="links">
                        <?php echo $links; ?>
                    </div>
                    <div class="slides">
                        <?php echo $slides; ?>
                    </div>

                    <button type="submit" name="btnSubmit" class="btnSubmit">Submit</button>
                </div>
            </form>
        </main>
    </body>
</html>