<?php
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    $credsUsername = "root";
    $credsPassword = "";

    $server = "localhost";
    $database   = "quizDB";

    
    $con = new mysqli($server, $credsUsername, $credsPassword, $database);
    if ($con->connect_error) {
        die("Error connecting: " . $con->connect_error);
    }