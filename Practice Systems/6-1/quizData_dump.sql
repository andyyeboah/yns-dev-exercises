CREATE DATABASE `quizDB`;
USE `quizDB`;


CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `choice_1` text NOT NULL,
  `choice_2` text NOT NULL,
  `choice_3` text NOT NULL,
  `choice_4` text NOT NULL,
  `answer` int(11) NOT NULL
);

INSERT INTO `data` (`id`, `question`, `choice_1`, `choice_2`, `choice_3`, `choice_4`, `answer`) VALUES
(1, 'What has to be broken before you can use it?', 'Heart', 'Egg', 'Glass', 'Bullet', 2),
(2, 'I’m tall when I’m young, and I’m short when I’m old. What am I?', 'Baby', 'Human', 'Light Saber', 'Candle', 4),
(3, 'What month of the year has 28 days?', 'Feb', 'Mar', 'Apr', 'All', 4),
(4, 'What is full of holes but still holds water?', 'Bucket', 'Shower', 'Sponge', 'Pineapple', 3),
(5, 'What goes up but never comes down?', 'Age', 'Sun', 'Clock', 'Blood Pressure', 1),
(6, 'So cute...', 'You', 'Your Ex', 'Your teacher', 'No one', 1),
(7, 'Which do you think is the answer?', 'This is the answer', 'Pick me. I am the answer', 'Hey! It is me', 'Well, I should be the answer', 4),
(8, 'Highest thing on earth', 'Ego', 'Building', 'Giraffe', 'Mountain', 4),
(9, 'A man dies of old age on his 25th birthday. How is this possible?', 'His bday is Feb 29', 'Ewan', 'Not sure', 'Choice number 2', 1),
(10, ' I shave every day, but my beard stays the same. What am I?', 'Magician', 'Barber', 'Politician', 'Actor', 2);

ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

