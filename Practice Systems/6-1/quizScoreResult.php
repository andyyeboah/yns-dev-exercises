<?php
    include "quizConnection.php";
    $key = [];
    $num = 0;
    if(isset($_POST["btnSubmit"])) {
        for ($i=1; $i < 5; $i++) { 
            $query = $con->prepare("SELECT * FROM `data` WHERE `id`=?");
            $query->bind_param("i", $_POST["qid".$i]);
            $query->execute();
            $response = $query->get_result();
            while ($row = $response->fetch_assoc()){
                for ($x=1; $x < 5; $x++) {
                    if ($_POST["answer".$i] == $row["choice_".$x]) {
                        ($row["answer"] == $x)? $num++:"";
                    }
                }
            }
            $query->close();
        }
    }
    $con->close();
?>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <main>
            <div class="title">
                    <hr>
                    <h1>RIDDLEY RIDDLE QUIZ</h1>
                    <hr>
                    <h3>HERE'S HOW IT WENT</h3>
                <h1 class="txtScore">Score: <?php echo ($num*25)."%"; ?></h1>
                <div class="btnWrapper">
                    <a href="index.php" class="btnHalf btnQuiz">Take the Quiz Again?</a>
                </div>
            </div>
        </main>
    </body>
</html>