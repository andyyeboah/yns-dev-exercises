<html>
	<head>
		<style>
            a{
                text-decoration: none;
                cursor: pointer;
                color: blue;
            }
            #mmYYYY{
                color: black;
            }
			.dateCurrent{
                background: black;
                color: white;
			}
		</style>
	</head>
	<body>
		<div>
			<main>
                <div>
                    <nav aria-label="Pagination">
                        <a onclick="goBack()" >
                            <span >Previous&emsp;</span>
                        </a>
                        <a id="mmYYYY" aria-current="page"></a>
                        <a onclick="goNextPage()">
                            <span>&emsp;Next</span>
                        </a>
                    </nav>
                    <br>
                    <table v-if="displayedPosts.length > 0">
                        <thead>
                            <tr>
                                <th scope="col">
                                    SUN
                                </th>
                                <th scope="col">
                                    MON
                                </th>
                                <th scope="col">
                                    TUES
                                </th>
                                <th scope="col">
                                    WED
                                </th>
                                <th scope="col">
                                    THURS
                                </th>
                                <th scope="col">
                                    FRI
                                </th>
                                <th scope="col">
                                    SAT
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                    </table>
                </div>
			</main>
		</div>
	</body>
	<script>
		monthArray = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

        
		currentDay = new Date();
		currentMonth = currentDay.getMonth();
		currentYear = currentDay.getFullYear();
        mmYYYY = document.getElementById("mmYYYY");
		displayTable(currentMonth, currentYear);

        function getNumberOfDays(month, year) {
			return 32 - new Date(year, month, 32).getDate();
		}
		
        function displayTable(month, year) {
		
        body = document.getElementById("table"); 
        body.innerHTML = "";
        mmYYYY.innerHTML = monthArray[month] + " " + year;

        let date = 1;
        let day = (new Date(year, month)).getDay();
            for (let iterator1 = 0; iterator1 < 6; iterator1++) {
                let row = document.createElement("tr");

                for (let iterator2 = 0; iterator2 < 7; iterator2++) {
                    if (iterator1 === 0 && iterator2 < day) {
                        singleItem = document.createElement("td");
                        singleText = document.createTextNode("");
                        singleItem.appendChild(singleText);
                        row.appendChild(singleItem);
                    }
                    else if (date > getNumberOfDays(month, year)) {
                        break;
                    }

                    else {
                        singleItem = document.createElement("td");
                        singleText = document.createTextNode(date);
                        if (date === currentDay.getDate() && year === currentDay.getFullYear() && month === currentDay.getMonth()) {
                            singleItem.classList.add("dateCurrent");
                        } 
                        singleItem.appendChild(singleText);
                        row.appendChild(singleItem);
                        date++;
                    }
                }
                body.appendChild(row); 
            }
        }

		function goNextPage() {
			currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
			currentMonth = (currentMonth + 1) % 12;
			displayTable(currentMonth, currentYear);
		}

		function goBack() {
			currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
			currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
			displayTable(currentMonth, currentYear);
		}
	</script>
</html>
