<html>
  <body>
    <form method = "post">
        <h4>Instructions: Indicate two numbers on the input fields, and select the "GET GCD" button to compute for Greatest Common Divisor.</h4>
        First Number: <input type = "number" name = "firstInput" required> <br>
        Second Number: <input type = "number" name = "secondInput" required> <br>
	  <input type = "submit" name = "submit" value = "GET GCD">
	</form>
	<?php
        // THERE IS A SHORTHAND FOR CALCULATING THE GCD USING gmp_gcd, BUT IT WILL THROW AN ERROR IF THE GMP IS NOT INSTALLED IN TH CODE REVIEWER'S PC
        function getGCD($input1, $input2){
            if ($input1 == 0 && $input2 == 0){
                echo "0";
            }
            if ($input1 == 0){
                return $input2;
            }
            echo getGCD($input2%$input1, $input1);

        }
	    if(isset($_POST['submit'])) {
		    $firstInput = $_POST['firstInput'];		
		    $secondInput = $_POST['secondInput'];
            getGCD($firstInput, $secondInput);          
		}
	?>
  </body>
</html>