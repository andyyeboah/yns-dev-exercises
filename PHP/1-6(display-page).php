<?php
    $nameFirst = htmlspecialchars($_POST["nameFirst"]);
    $nameMiddle = htmlspecialchars($_POST["nameMiddle"]);
    $nameLast = htmlspecialchars($_POST["nameLast"]);
    $nickname = htmlspecialchars($_POST["nickname"]);
    $birthdate = new DateTime($_POST["birthdate"]);
    $email = htmlspecialchars($_POST["email"]);

    echo "<strong>Name:</strong> $nameFirst $nameMiddle $nameLast"." (".$nickname.") <br>";
    echo "<strong>Date of Birth:</strong> ".$birthdate->format("m-d-Y")."<br>";
    echo "<strong>Email Address:</strong> $email <br>";
?>
