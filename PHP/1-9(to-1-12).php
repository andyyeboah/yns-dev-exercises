<?php
	session_start();
	if(!isset($_SESSION['currentUser'][0])){
		header("Location:1-13.php");
	}

    $filename = 'users.csv';
    $currentFile = fopen($filename, 'r');
	if ($currentFile === false) {
		echo "'Cannot open file'.$filename";
	}

    $userInput = [];
	while (($file = fgetcsv($currentFile)) !== false) {
		$userInput[] = $file;
	}

    $listPerPage = 10;
    $pageCurrent = isset($_GET['pageNumber']) ? (int)$_GET['pageNumber']:1;
    $pagePrevious = $pageCurrent - 1;
    $pageNext = $pageCurrent + 1;
	$pageCount = ceil((count($userInput)/$listPerPage));
	$start = ($pageCurrent * $listPerPage) - $listPerPage;
	$end = ($pageCurrent * $listPerPage);
	$userInput = array_slice($userInput, $start, $end);


	fclose($currentFile);
?>
<!doctype html>
<html>
	<body>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div>
            <table v-if="displayedPosts.length > 0">
                    <thead>
                        <tr>
                            <th scope="col">
                                User Name
                            </th>
                            <th scope="col">
                                Email Address
                            </th>
                            <th scope="col">
                                Age
                            </th>
                            <th scope="col">
                                Password
                            </th>
                            <th scope="col">
                                Image
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(count($userInput) > 0){
                                for($iterator1=0; $iterator1 < count($userInput); ++$iterator1){
                                    echo "<tr>";
                                    for($iterator2=0; $iterator2 < sizeof($userInput[$iterator1]); $iterator2++){
                                        if($iterator2 == (sizeof($userInput[$iterator1]) - 1)){
                                            echo '<td>';
                                                echo '<div>';
                                                    echo '<div>';
                                                        echo "<img src='photos/".$userInput[$iterator1][$iterator2]."' alt=''>";
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</td>';
                                        } else {
                                            echo '<td>'.$userInput[$iterator1][$iterator2]."</div></td>";
                                        }
                                    }
                                    echo "</td>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
                <div>
                    <nav aria-label="Pagination">
                        <?php
                            if($pageCurrent > 1){
                                echo '<a href="?pageNumber='.($pageCurrent-1).'">'.
                                    '<span>Previous</span>'.'</a>';
                            }

                            for($iterator1=1; $iterator1 <= $pageCount; $iterator1++){
                                echo '<a href="?pageNumber='.$iterator1.'">'.$iterator1.'</a>';
                            }

                            if($pageCurrent < $pageCount){
                                echo '<a href="?pageNumber='.($pageCurrent+1).'">'.
                                    '<span class="sr-only">Previous</span>'.'</a>';
                            }
                        ?>
                    </nav>
                </div>
            </div>
        </form>
        <a href="end-session.php">Logout</a>
	</body>
</html>
