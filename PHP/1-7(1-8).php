<?php
	$userInput = array();
	$validate = array();
	$userPhoto = "";

	if(isset($_POST['submit']))
	{
    foreach($_POST as $key => $input){
			if(empty($input)){
				$validate[$key] = $key." is a required field.";
			} else {
				if($key == "name"){
          if(!preg_match('/[a-zA-z]+(\s?([a-zA-a]*))/', $input)){
            $validate[$key] = $key." must only contain letters and spaces";
          }
        }
        if($key == "email"){
          if(!preg_match("/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(((\.[c][o][m])|((\.[^c][^o][^m]?)([a-z0-9-]*[a-z0-9])?)|(\.[c])|((\.[^c][^o]?)([a-z0-9-]*[a-z0-9])?)|(\.[c][o][a-z0-9-]?)|(\.[c][o][^m]([a-z0-9-]*[a-z0-9])?)|(\.[c][^o]([a-z0-9-]*[a-z0-9])?))+$)/i", $input)){
            $validate[$key] = "invalid email address.";
          }
        }
        if($key == "password"){
          if(strlen($input) < 8){
            $validate[$key] = $key." must have at least 8 characters.";
          }
        }
			}
		}

		if(isset($_FILES['image'])){

			$userPhoto = $_FILES['image']['name'];
			$file_temp = $_FILES['image']['tmp_name'];
			$extension = explode('.',$userPhoto);
			$fileExtension = strtolower(end($extension));
			$allowedExtension = array("jpeg","jpg","png");

			if(in_array($fileExtension,$allowedExtension) === false){
				$validate['image']="Please, choose an image and make sure it is in the ff format: jpeg, jpg, or png.";
			} else {
				if(count($validate) <= 0){
					move_uploaded_file($file_temp,"photos/".$userPhoto);
				}
			}
		} else {
			$validate['image'] = "Please, upload an image.";
		}

		if(count($validate) <= 0 ){
			$filename = 'users.csv';
			$open_file = fopen($filename, 'a');
			array_pop($_POST);
			array_push($_POST, $userPhoto);
			if(!$open_file){
				echo "There is an exception while opening the file.";
			} else {
				fputcsv($open_file,$_POST);
			}
			fclose($open_file);
			session_start();
			$_SESSION["currentUser"] = $_POST;
			header("Location:1-9(to-1-12).php");
		}
	}
?>
<!doctype html>
<html>
	<body>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data"> 
      <div>
        <div>
            <label for="form-validation"> Fullname </label>
            <div>
            <input type="text" name="name" id="form-validation" >
            </div>
        </div>
        <div>
            <label for="form-validation"> Email Address </label>
            <div>
            <input type="email" name="email">
            </div>
        </div>
        <div>
            <label for="form-validation" > Age </label>
            <div>
            <input type="number" name="age">        
            </div>
        </div>
        <div>
            <label for="form-validation" > Password </label>
            <div>
            <input type="password" name="password">
            </div>
        </div>
        <div>
            <label for="form-validation" > Photo </label>
            <div>
            <input type="file" name="image">
            </div>
        </div>
        <?php
          if(count($validate) > 0){
            echo '<div>Please, fill out the form with appopriate format.</div>';
              echo '<ul">';
              foreach($validate as $err){
                echo "<li>$err</li>";
              }
              echo "</ul>";
            echo '</div>';
          }
        ?>
        <div>
          <input type="submit" name="submit" value="Submit">
        </div>
        </div>
      </div>
    </form>
	</body>
</html>
