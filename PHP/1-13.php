<?php
	$validate = array();
	$isUserValid = false;
	$validUsers = array();
	if(isset($_POST['submit']))
	{
		foreach($_POST as $key => $input){
			if(empty($input)){
				$validate[$key] = $key." is a required field.";
			}
		}

		if(count($validate) <= 0){
            $filename = 'users.csv';
			$currentFile = fopen($filename, 'r');
			if ($currentFile === false) {
				echo "Cannot open file' . $filename";
			}

			while (($file = fgetcsv($currentFile)) !== false) {
				if($file[0] == $_POST['userID'] && $file[3] == $_POST['password']){
					$isUserValid = true;
					$validUsers = $file;
					break;
				}
			}

			if($isUserValid){
				session_start();
				$_SESSION['currentUser'] = $validUsers;
				header("Location:1-9(to-1-12).php?");
			} else {
				$validate['attempt'] = "Invalid ID or Password";
			}
		}
	}
?>

<!doctype html>
<html>
	<body>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div>
                <div>
                    <label for="form-validation"> Username </label>
                    <div>
                        <input type="text" name="userID" placeholder="Your Fullname">
                    </div>
                </div>
                <div>
                    <label for="form-validation"> Password </label>
                    <div>
                        <input type="password" name="password">
                    </div>
                </div>
                <?php
                    if(count($validate) > 0){
                        foreach($validate as $err){
                            echo '<div>'.$err.'</div>';
                        }
                    }
                ?>
                <div>
                    <input type="submit" name="submit" value="Login" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                </div>
            </div>
        </form>
	</body>
</html>
