CREATE DATABASE `exercise3-5`;
USE `exercise3-5`;

CREATE TABLE `users` (
    `email` varchar(100) DEFAULT NULL,
    `user_password` varchar(100) DEFAULT NULL,
    `first_name` varchar(100) DEFAULT NULL,
    `last_name` varchar(100) DEFAULT NULL,
    `birth_date` date DEFAULT NULL,
    `user_image` tinytext DEFAULT NULL
);

INSERT INTO `users` (email, user_password, first_name, last_name, birth_date, user_image) 
VALUES 
    (' andy@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy1@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy2@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy3@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy4@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy5@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy6@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy7@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy8@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    (' andy9@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah1@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah2@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah3@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah4@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah5@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah6@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah7@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg'),
    ('andyyeboah8@gmail.com', 'password123', 'Yeboi', 'Cudjoe', '1999-01-01', 'london.jpg');




