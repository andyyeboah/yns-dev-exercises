-- START OF CREATING TABLES AND INSERTING DATA

CREATE DATABASE `managementdb`; 
USE `managementdb`;

    -- EMPLOYEES TABLE
CREATE TABLE `employees` (
	id INT auto_increment NOT NULL,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NULL,
	middle_name varchar(100) NULL,
	birth_date DATE NOT NULL,
	department_id INT NULL,
	hire_date DATE NULL,
	boss_id INT NULL,
	CONSTRAINT employees_pk PRIMARY KEY (id)
);

INSERT INTO `employees` (first_name,last_name,birth_date,department_id)
	VALUES ('Manabu','Yamazaki','1976-03-15',1);
INSERT INTO `employees` (first_name,last_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Tomohiko','Takasago','1974-05-24',3,'2014-04-01',1);
INSERT INTO `employees` (first_name,last_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Yuta','Kawakami','1990-08-13',4,'2014-04-01',1);
INSERT INTO `employees` (first_name,last_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Shogo','Kubota','1985-01-31',4,'2014-12-01	',1);
INSERT INTO `employees` (first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Lorraine','San Jose','P.','1983-10-11',2,'2015-03-10',1);
INSERT INTO `employees` (first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Haille','Dela Cruz','A.','1990-11-12',3,'2015-02-15',2);
INSERT INTO `employees` (first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Godfrey','Sarmenta','L.','1993-09-13',4,'2015-01-01',1);
INSERT INTO `employees` (first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Alex','Amistad','F.','1988-04-14',4,'2015-04-10',1);
INSERT INTO `employees` (first_name,last_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Hideshi','Ogoshi','1983-07-15',4,'2014-06-01',1);
INSERT INTO `employees` (first_name,birth_date,department_id,hire_date,boss_id)
	VALUES ('Kim','1977-10-16',5,'2015-08-06',1);

    -- DEPARTMENTS TABLE
CREATE TABLE `departments` (
	id INT auto_increment NOT NULL,
	name varchar(100) NOT NULL,
	CONSTRAINT departments_pk PRIMARY KEY (id)
);

INSERT INTO `departments`
(id, name)
VALUES(1, 'Executive');
INSERT INTO `departments`
(id, name)
VALUES(2, 'Admin');
INSERT INTO `departments`
(id, name)
VALUES(3, 'Sales');
INSERT INTO `departments`
(id, name)
VALUES(4, 'Development');
INSERT INTO `departments`
(id, name)
VALUES(5, 'Design');
INSERT INTO `departments`
(id, name)
VALUES(6, 'Marketing');

    -- POSITIONS TABLE
CREATE TABLE `positions` (
	id INT auto_increment NOT NULL,
	name varchar(100) NOT NULL,
	CONSTRAINT positions_pk PRIMARY KEY (id)
);

INSERT INTO `positions` (name)
	VALUES ('CEO');
INSERT INTO `positions` (name)
	VALUES ('CTO');
INSERT INTO `positions` (name)
	VALUES ('CFO');
INSERT INTO `positions` (name)
	VALUES ('Manager');
INSERT INTO `positions` (name)
	VALUES ('Staff');

    -- EMPLOYEE POSITIONS TABLE
CREATE TABLE `employee_positions` (
	id INT auto_increment NOT NULL,
	employee_id INT NOT NULL,
	position_id INT NOT NULL,
	CONSTRAINT employee_positions_pk PRIMARY KEY (id)
);

INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (1,1);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (1,2);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (1,3);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (2,4);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (3,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (4,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (5,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (6,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (7,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (8,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (9,5);
INSERT INTO `employee_positions` (employee_id,position_id)
	VALUES (10,5);

-- END CREATING TABLES AND INSERTING DATA




-- START OF EXECERCISES

    -- EXERCISE 1
USE `managementdb`;
SELECT id, first_name, last_name, middle_name, department_id, hire_date, boss_id
FROM employees  
WHERE last_name LIKE 'k%'

    -- EXERCISE 2
USE `managementdb`;
SELECT id, first_name, last_name, middle_name, department_id, hire_date, boss_id
FROM employees  
WHERE last_name LIKE '%i'

    -- EXERCISE 3
USE `managementdb`;
SELECT CONCAT(first_name, " ", middle_name, " ",  last_name) as full_name, hire_date
FROM employees
WHERE hire_date BETWEEN "2015-01-01" AND "2015-03-31"
ORDER BY hire_date ASC;

    -- EXERCISE 4
USE `managementdb`;
SELECT e.last_name AS Employee, b.last_name AS Boss
FROM employees e
INNER JOIN employees b ON e.boss_id = b.id
ORDER BY e.id;

    -- EXERCISE 5
USE `managementdb`;
SELECT e.last_name
FROM employees e
INNER JOIN departments d ON d.id = e.department_id
WHERE d.name = 'Sales'

    -- EXERCISE 6
USE `managementdb`;
SELECT COUNT(middle_name) AS count_has_middle
FROM employees
WHERE LENGTH(middle_name)>=1;
	
    -- EXERCISE 7
USE `managementdb`;
SELECT UPPER(d.name), COUNT(e.department_id)
FROM departments d
JOIN employees e ON d.id=e.department_id
GROUP BY d.name
ORDER BY d.id;

	-- EXERCISE 8
USE `managementdb`;
SELECT first_name, middle_name, last_name, hire_date
FROM employees
ORDER BY hire_date ASC 
LIMIT 1;

    -- EXERCISE 9
USE `managementdb`;
SELECT  d.name
FROM departments d
WHERE d.id NOT IN (select department_id FROM employees);

    -- EXERCISE 10
USE `managementdb`;
SELECT e.first_name, e.middle_name, e.last_name
FROM employees e
JOIN employee_positions ep ON e.id = ep.employee_id
GROUP BY ep.employee_id
HAVING COUNT(ep.position_id)>2;
 


