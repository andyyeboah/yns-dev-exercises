CREATE DATABASE `management`;
USE `management`;
CREATE TABLE `students` (
	id INT auto_increment NOT NULL,
	employee_name varchar(100) NULL,
	birthdate DATE NULL,
	CONSTRAINT employees_pk PRIMARY KEY (id)
)