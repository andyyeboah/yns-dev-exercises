/*INSERT*/
USE `management`;
INSERT INTO `students`
(id, employee_name, birthdate)
VALUES(1, 'Andy Yeboah Cudjoe', '1999-11-19');
INSERT INTO `students`
(id, employee_name, birthdate)
VALUES(2, 'Juan Miguel Agapay', '2000-02-02');


/*UPDATE*/
USE `management`;
UPDATE `students`
	SET employee_name='Ronniel Viray'
	WHERE id=2;

/*DELETE*/
USE `management`;
DELETE FROM `students`
WHERE id=2;

