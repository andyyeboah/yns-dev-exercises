<?php
    session_start();
?>
<html>
  <body>
    <table>
      <tr>
        <th><strong>USERS</strong></th>
        <th><strong>PROFILE</strong></th>
      </tr>
      <?php

        $credsUsername = 'root';
        $credsPassword = '';
        
        $serverHost = 'localhost';
        $database = 'exercise3-5';

        $connection = new mysqli($serverHost, $credsUsername, $credsPassword, $database);
        $query = $connection->query("SELECT * FROM users;");

        $displayedUsers = $query->num_rows;

          $page;
          if (!empty($_GET['pointer'])) {
            $page = (int) $_GET['pointer'];
          } else {
            $page = 1;
          }
          if ($page < 1) {
            $page = 1;
          }

          $usersDisplayed = 10;
          if ($page + $usersDisplayed > $displayedUsers) {
            $page = $displayedUsers - $usersDisplayed + 1;
          }

          $line = 1; 
          $displayedRows = 0;
          $query = $connection->query("SELECT email, user_image FROM users;");

          while($data = $query->fetch_assoc()) {
            if ($displayedRows == $usersDisplayed) {
              break;
            }

            if ($line >= $page) {
              echo '<tr>';
              $field = $data['email'];
              echo "<td> $field </td>";
              $image = 'images/' . $data['user_image'];
              echo "<td> <img src='$image' height='50'> </td>";
              echo '</tr>';
              $displayedRows++;
            }

            $line++;

          }
          echo '</table>';
          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($page - $usersDisplayed) . '>Previous</a> ';
          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($page + $usersDisplayed) . '>Next</a><br>';
    ?>
    <br><br><a href=3-5(login).php>Logout</a>
  </body>
</html>