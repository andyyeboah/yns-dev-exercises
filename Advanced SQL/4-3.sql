USE `managementdb`;
SELECT e.first_name,e.last_name, e.middle_name, e.birth_date,
    CASE
        WHEN p.name = "CEO" THEN "Chief Executive Officer"
        WHEN p.name = "CTO" THEN "Chief Technical Officer"
        WHEN p.name = "CFO" THEN "Chief Financial Officer"
        ELSE p.name
    END AS position_name
FROM employees e
INNER JOIN employee_positions ep ON ep.employee_id = e.id
INNER JOIN positions p ON p.id = ep.position_id