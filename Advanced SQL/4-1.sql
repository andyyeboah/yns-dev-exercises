USE `managementdb`;
SELECT  e.first_name, e.last_name, e.birth_date 
FROM employees e 
WHERE  birth_date 
	BETWEEN CURDATE() - INTERVAL 40 YEAR and CURDATE() - INTERVAL 30 YEAR