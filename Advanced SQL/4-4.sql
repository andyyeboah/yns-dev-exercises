-- SQL DUMP START 
CREATE DATABASE `exercise4-4`;

USE `exercise4-4`;

CREATE TABLE `master_data` (
  `id` int(11),
  `parent_id` int(11) DEFAULT NULL
);

INSERT INTO `master_data`
(id, parent_id)
VALUES(1, NULL);
INSERT INTO `master_data`
(id, parent_id)
VALUES(2, 5);
INSERT INTO `master_data`
(id, parent_id)
VALUES(3, NULL);
INSERT INTO `master_data`
(id, parent_id)
VALUES(4, 1);
INSERT INTO `master_data`
(id, parent_id)
VALUES(5, NULL);
INSERT INTO `master_data`
(id, parent_id)
VALUES(6, 3);
INSERT INTO `master_data`
(id, parent_id)
VALUES(7, 3);
INSERT INTO `master_data`
(id, parent_id)
VALUES(NULL, NULL);
-- SQL DUMP END 

-- SOLUTION START
SELECT sortable.id, sortable.parent_id
FROM (SELECT id, parent_id, 
    CASE 
        WHEN NOT (parent_id IS NULL OR id<parent_id OR parent_id=(id/2)) 
            THEN (id-parent_id-1) 
        WHEN NOT (parent_id IS NULL OR id<parent_id OR parent_id!=(id/2)) 
            THEN (id-parent_id) 
        WHEN NOT (parent_id IS NULL OR id>parent_id) 
            THEN (parent_id-id) 
        ELSE id 
    END
    AS arranged_asc
FROM master_data
WHERE id IS NOT NULL) sortable
ORDER BY arranged_asc, parent_id, id ASC;