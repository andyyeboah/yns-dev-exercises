-- SQL DUMP START

CREATE DATABASE `therapy-manager`;
USE `therapy-manager`;

CREATE TABLE `daily_work_shifts` (
  `id` int(1) DEFAULT NULL,
  `therapist_id` int(1) DEFAULT NULL,
  `target_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
);

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) 
VALUES
    (1, 1, '2022-03-17', '14:00:00', '15:00:00'),
    (2, 2, '2022-03-17', '22:00:00', '23:00:00'),
    (3, 3, '2022-03-17', '00:00:00', '01:00:00'),
    (4, 4, '2022-03-17', '05:00:00', '05:30:00'),
    (5, 1, '2022-03-17', '21:00:00', '21:45:00'),
    (6, 5, '2022-03-17', '05:00:00', '05:20:00'),
    (7, 3, '2022-03-17', '14:00:00', '15:00:00');

CREATE TABLE `therapists` (
  `id` int(1) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL
);

INSERT INTO `therapists` (`id`, `name`) 
VALUES
    (1, 'John'),
    (2, 'Arnold'),
    (3, 'Robert'),
    (4, 'Ervin'),
    (5, 'Smith');

-- SQL DUMP END


-- EXCERCISE SOLUTION
USE `therapy-manager`;
SELECT therapist_id,target_date,start_time,end_time,
    CASE
        WHEN start_time <= '05:59:59' AND start_time >= ' 00:00:00'
        THEN CONCAT(target_date + interval 1 day, ' ' , start_time)
        ELSE CONCAT(target_date , ' ' , start_time)
    END sort_start_time
FROM daily_work_shifts dwf
INNER JOIN therapists th on th.id = therapist_id
ORDER BY target_date ASC, sort_start_time ASC;